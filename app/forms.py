from app.database import User, session
from sqlalchemy.sql import select, or_

from config.auth import BaseAuth


RULES = {
    'name': {
        'min_length': 3
    },
    'password': {
        'min_length': 6
    }
}


class GeneralForm(object):
    """Main class to validate if the a form is valid."""

    def __init__(self):
        self.errors = None

    def is_valid(self):
        """Return True if the form has no errors."""
        if self.errors is None:
            return True


class RegisterForm(GeneralForm):

    def validate_form(self, form):
        """
        Validate if the register form comply
        with the security established rules.
        """
        messages = []
        for key, value in form.items():
            try:
                min_length = RULES[key]['min_length']
                if len(value) < min_length:
                    error_message = '{} is too short'.format(key)
                    messages.append({'message': error_message})
                    self.errors = True
            except KeyError as e:
                print (type(e), e)

        self.clean_username(form['email'], messages)
        return messages

    def clean_username(self, username, messages):
        """
        Validate if the email entered in the register form
        already exists.
        """
        query = select([User.email]).where(User.email == username)
        execution = session.execute(query)
        results = execution.fetchone()
        if results:
            self.errors = True
            return messages.append({'message': 'User already exists'})


class LoginForm(GeneralForm):

    def validate_form(self, form):
        """
        Validate in the username and password match or if
        the user exists in the database.
        """
        messages = []
        email = form['email']
        password = form['password']

        query = select([User.email, User.password]).where(User.email == email)
        execution = session.execute(query)
        results = execution.fetchone()
        if not results:
            self.errors = True
            messages.append({'message': 'User does not exists'})
            return messages

        check_password = BaseAuth.check_password(form['password'], results[1])
        if not check_password[0] == check_password[1]:
            self.errors = True
            messages.append({'message': 'Password incorrect'})
            return messages
