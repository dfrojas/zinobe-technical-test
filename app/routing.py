from werkzeug.routing import Map, Rule


url_map = Map([
    Rule('/', endpoint='index'),
    Rule('/login', endpoint='login'),
    Rule('/register', endpoint='register'),
    Rule('/search', endpoint='search'),
    Rule('/logout', endpoint='logout'),
])
