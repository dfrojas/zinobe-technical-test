from werkzeug.utils import redirect


def if_is_logged_in(redirect_to):
    """
    Decorator to redirect a user to any url
    when is authenticated and we want
    that the user does not go the decorated view.
    For example, an authenticated user try to access
    to the login page.

    Parameter:
        redirect_to: Relative path of the desired url.
    """
    def wrap(function):
        def wrapped_inner(request, *args):
            if request.user() is not None:
                return redirect(redirect_to)
            return function(request, *args)
        return wrapped_inner
    return wrap

def login_required(func):
    """
    Decorator to protect a function or class
    when require the user is authenticated.

    Takes path of the protected page to redirect the user
    after the login.
    """
    def wrapper(request, *args):
        if request.user() is None:
            return redirect('/login?redirect_to={}'.format(request.path))
        return func(request, *args)
    return wrapper
