from sqlalchemy import (
	Column,
	String,
	Integer,
	Date,
	MetaData,
	create_engine)

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import event


metadata = MetaData()
Base = declarative_base()


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    email = Column(String)
    country = Column(String)
    password = Column(String)


class Country(Base):
	__tablename__ = 'countries'

	id = Column(Integer, primary_key=True)
	name = Column(String)
	code = Column(String)

	def populate(self):
		self.name = "CODE"


engine = create_engine('postgresql://zinobe:zinobe@localhost/zinobe', echo=True)
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()


def insert_initial_values(*args, **kwargs):
	create_user = Country(name="name", code="code")
	session.add(create_user)
	"""
    package = Package('https://datahub.io/core/country-list/datapackage.json')
    countries = []
    Since there are only 198 countries in the world, we could
    implement here a O(n).

    for resource in package.resources:
        if resource.descriptor['datahub']['type'] == 'derived/csv':
            for country in resource.data:
                countries.append(country)
    """
	#session.add(Country(code='low'))
	session.commit()

event.listen(Country.__table__, 'after_create', insert_initial_values)
# event.listen(MyClass.collection, 'append', my_append_listener)







