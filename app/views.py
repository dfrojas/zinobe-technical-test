import os
import sys
import random
import string

from http import cookies

from werkzeug.wrappers import Request, Response
from werkzeug.http import dump_cookie
from werkzeug.utils import cached_property, redirect

from app.forms import RegisterForm, LoginForm
from app.decorators import login_required, if_is_logged_in
from config.auth import BaseAuth
from utils import insert_database


class MapperViews(Request, BaseAuth):
    """Handles all the pages of the application."""

    def index(self, request):
        from config.templates import ConfigurationTemplate
        template = ConfigurationTemplate("index.html").render_template()
        return Response(template.render(request=request), mimetype='text/html')

    @if_is_logged_in('/search')
    def login(self, request):
        from config.templates import ConfigurationTemplate
        template = ConfigurationTemplate("login.html").render_template()
        errors = []

        if request.method == 'POST':
            email = request.form['email']
            password = request.form['password']

            form = LoginForm()
            validate = form.validate_form(request.form.to_dict())
            if form.is_valid():
                request.login(email)
                if request.args.get('redirect_to'):
                    return redirect(request.args.get('redirect_to'))
                else:
                    return redirect('/search')
            else:
                errors = validate

        return Response(template.render(request=request, errors=errors), mimetype='text/html')

    @if_is_logged_in('/search')
    def register(self, request):
        from config.templates import ConfigurationTemplate
        errors = []
        headers = None

        if request.method == 'POST':
            name = request.form['name']
            email = request.form['email']
            country = request.form['country']
            password = request.form['password']

            form = RegisterForm()
            clean = form.validate_form(request.form.to_dict())
            if form.is_valid():
                encrypted_password = BaseAuth.set_password(password)
                insert_database(name, email, country, encrypted_password)
                request.login(email)
                return redirect('/search')
            else:
                errors = clean

        template = ConfigurationTemplate("register.html").render_template()
        return Response(template.render(request=request, errors=errors), mimetype='text/html', headers=headers)

    @login_required
    def search(self, request):
        from config.templates import ConfigurationTemplate
        from sqlalchemy.sql import select, or_
        from sqlalchemy import column, table
        from app.database import User, session

        results = None

        if request.args.get('q'):
            term = "%{}%".format(request.args.get('q'))
            query = select([User.name, User.country, User.email]).where(or_(User.name.ilike(term), User.email.ilike(term)))
            execution = session.execute(query)
            results = execution.fetchall()
            
        template = ConfigurationTemplate("search.html").render_template()
        return Response(template.render(request=request, results=results), mimetype='text/html')

    def logout(self, request):
        request.logout()
        return redirect('/')

