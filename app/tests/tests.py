import unittest
from unittest.mock import MagicMock, patch

from werkzeug.wrappers import Request, Response
from werkzeug.test import EnvironBuilder, create_environ
from werkzeug.wrappers import BaseResponse

from app.views import MapperViews
from config.server import StartServer
from config.auth import BaseAuth


@patch('config.server.StartServer')
class TestLogin(unittest.TestCase):

    def setUp(self):
        env = create_environ('/', 4000)
        self.mapper = MapperViews(env, True)

    def test_login_no_authenticated(self, request):
        """
        if user is not authenticated, should see the
        login page normally.
        """
        self.mapper.session = {'username': None}
        response = self.mapper.login(request)
        self.assertEqual(response.status_code, 200)

    def test_login_authenticated(self, request):
        """
        Test to ensure that the user is redirected
        to other URL when is authenticated when try to
        access to login page.
        """
        self.mapper.session = {'username': 'dummyuser@zinobe.com'}
        response = self.mapper.login(request)
        self.assertEqual(response.status_code, 302)


@patch('config.server.StartServer')
class TestSearch(unittest.TestCase):

    def setUp(self):
        env = create_environ('/', 4000)
        self.mapper = MapperViews(env, True)

    def test_fordibben_access(self, request):
        """
        Test case when a user is not authenticated the user
        is redirected to the login page.
        """
        self.mapper.session = {'username': None}
        response = self.mapper.search(request)
        self.assertEqual(response.status_code, 302)

    def test_allowed_access(self, request):
        """
        Test when a user is authenticated and can
        access to the search user form.
        """
        self.mapper.session = {'username': 'user@user.com'}
        response = self.mapper.search(request)
        self.assertEqual(response.status_code, 200)


@patch('config.server.StartServer')
class TestPasswordHash(unittest.TestCase):

    def setUp(self):
        env = create_environ('/', 4000)
        self.auth = BaseAuth()

    def test_check_password(self, request):
        """Test is a hashed password match with theplain text password."""
        dummy_password = '12345678'
        hashed_password = self.auth.set_password(dummy_password)
        check = self.auth.check_password(dummy_password, hashed_password)
        self.assertEqual(check[0], check[1])


if __name__ == '__main__':
    unittest.main()
