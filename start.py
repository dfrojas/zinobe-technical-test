from config.server import StartServer
from werkzeug.serving import run_simple


@StartServer.application
def application(request):
    """Raise the server"""
    response = request.dispatch_request(request)
    if request.session.should_save:
        session_data = request.session.serialize()
        response.set_cookie('username', session_data)
    return response

if __name__ == '__main__':
    run_simple('localhost', 4000, application, use_debugger=True, use_reloader=True)
