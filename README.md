## How to run

<p>1. Clone the repo</p>
<p>2. Install the requirements.txt</p>
<p>2. Create a postgresql (or any) database engine in the cloud or in your local environment. Please note if you run the application with another database different to postgresql you will need to install the corresponding library of Python.</p>
<p>3. Changes the database settings in database.py line 39 with the data of your database just created.</p>
<p>4. Run python database.py to create the database.</p>
<p>5. And that's all, now you only have to run python start.py to raise the server.</p>

## Run tests

<p>In order to avoid the common and tipical issue with the modules paths, we are using pytest. To run the tests you have to run pytest app/tests/tests.py</p>
