import random
import string

from werkzeug.exceptions import HTTPException
from werkzeug.wrappers import Request
from werkzeug.utils import cached_property
from werkzeug.contrib.securecookie import SecureCookie

from app.routing import url_map
from app.views import MapperViews
from config.auth import BaseAuth


SECRET_KEY = "SECRET_KEY" # ''.join(random.choices(string.ascii_letters + string.digits, k=32))


class StartServer(Request, BaseAuth):

    def dispatch_request(self, request):
        adapter = url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            return getattr(MapperViews, endpoint)(self, request, **values)
        except HTTPException as e:
            return e

    @cached_property
    def session(self):
        data = self.cookies.get('username')
        if not data:
            return SecureCookie(secret_key=SECRET_KEY)
        return SecureCookie.unserialize(data, SECRET_KEY)

    def wsgi_app(self, environ, start_response):
        request = Request(environ)
        response = self.dispatch_request(request)
        return response(environ, start_response)

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)
