import os
import sys
import jinja2

from jinja2 import Environment, FileSystemLoader, Template


class ConfigurationTemplate(object):
    """
    Base class to call the templates from files
    """
    def __init__(self, template_name):
        self.template_name = template_name

    def render_template(self):
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        template_path = os.path.join(base_dir, 'templates')
        template_loader = FileSystemLoader(template_path)
        template_env = Environment(loader=template_loader)
        template = template_env.get_template(self.template_name)
        return template
