import os
import hashlib


class BaseAuth(object):

    def user(self):
        """Return the user that is logged in."""
        return self.session.get('username')

    def login(self, username):
        """Login the user."""
        self.session['username'] = username

    def logout(self):
        """Destroy the session."""
        self.session.pop('username', None)

    @staticmethod
    def set_password(password):
        """
        Hash the password to stored in the
        database. Receive the plain text password
        as parameter. The final result is the
        password salt and the password hashed, separated
        by a dollar signal:<salt>$<hash>
        """
        password_salt = os.urandom(32).hex()
        hash = hashlib.sha512()
        hash.update(('%s%s' % (password_salt, password)).encode('utf-8'))
        password_hash = hash.hexdigest()
        encrypted = "{}${}".format(password_salt, password_hash)
        return encrypted

    @staticmethod
    def check_password(password, encrypted):
        """
        Check if the plain text password match with
        the hashed stored password.
        """
        password_salt, hashed_password = encrypted.split('$')
        hash = hashlib.sha512()
        hash.update(('%s%s' % (password_salt, password)).encode('utf-8'))
        password_hash = hash.hexdigest()
        return hashed_password, password_hash
