import random
import hashlib

from app.database import User, session


def insert_database(name, email, country, password):
    create_user = User(name=name, email=email, country=country, password=password)
    session.add(create_user)
    session.commit()
    return session


def handle_cookies(**kwargs):
    key = kwargs['key']
    value = ''.join(random.choices(string.ascii_letters + string.digits, k=32))
    max_age = kwargs['max_age']

    cookie = cookies.SimpleCookie()
    cookie[key] = value
    cookie[key]['max-age'] = max_age

    set_cookie = dump_cookie(key, cookie[key].value, cookie[key]['max-age'])
    return set_cookie